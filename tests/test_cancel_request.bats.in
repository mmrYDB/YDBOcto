#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
  load_fixture pastas.sql
  load_fixture pastas.zwr
  load_fixture default_user.zwr
  test_port=$(start_rocto 1441)
}

teardown() {
  stop_rocto
}

@test "ensure cancel requests abort complicated queries (many joins)" {
  yottadb -run ^%XCMD 'set ^%ydboctoocto("functions","HANGTIME")="$$^HANGTIME"'
  run_psql_expect cancel_psql_many_joins $test_port &> output.txt
  verify_output TCR01 expect.out noinfo nodebug noexpect stripreturns
}

@test "ensure cancel requests abort simple queries and cleanup xrefs and trigger" {
  load_big_data
  # Remove verbosity as the logical plan is too long to print correctly and not needed here
  run_psql_expect cancel_psql_big_data $test_port &> output.txt
  verify_output TCR00 expect.out noinfo nodebug noexpect stripreturns
  # Ensure all cross references cleaned up for the given query, i.e. test_seed_queries.bats "test1"
  run $ydb_dist/yottadb -direct > xref.txt <<YDB
zwrite ^%ydboctoxref
YDB
  echo $output > xref.txt
  run $ydb_dist/yottadb -direct >> xref.txt <<YDB
zwrite ^%ydboctoxref("NAMES","FIRSTNAME")
YDB
  echo $output >> xref.txt
  verify_output TCR02 xref.txt
  # Ensure all triggers are cleaned up, i.e. no triggers
  $ydb_dist/mupip TRIGGER -SELECT triggers.txt
  verify_output TCR03 triggers.txt
}

@test "ensure cancel requests abort extended queries" {
  # Skip until extended queries are supported
  skip
  test_port2=$(start_rocto 1441 quiet)
  run_psql_expect cancel_psql_extended $test_port2 &> output.txt
  verify_output TCR04 output.txt
}

