#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

FROM yottadb/yottadb-base:latest-master

# Separate, non-interactive installation of tzdata required due to expect's dependency on libtcl8.6, which depends on tzdata.
# If these steps aren't done, the build will open an interactive prompt to setup tzdata during apt-get install.
RUN export DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/US/Eastern /etc/localtime
RUN apt-get update -qq && \
    apt-get install -y tzdata
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt-get install -y -qq \
        build-essential \
        cmake \
        bison \
        flex \
        libcmocka-dev \
        python-pip \
        libreadline-dev \
        git \
        libconfig-dev \
        libssl-dev \
        postgresql-client \
        postgresql \
        xxd \
        wget
RUN pip install \
        sphinxcontrib-fulltoc \
        sphinx \
        sphinx_rtd_theme
ENV PATH=/usr/local/go/bin:$PATH
ENV GOLANG_VERSION=1.11.2
ENV USER=root
RUN wget -O go.tgz -q https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go.tgz
RUN rm go.tgz
RUN go version

ADD . /builds/YDBDBMS/
WORKDIR /builds/YDBDBMS

RUN tools/ci/build.sh
